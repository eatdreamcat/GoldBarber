import { LogicConst, GameLogic } from "../Controller/GameLogic";
import { GameStateMgr, GameState } from "../Controller/GameState";

const { ccclass, property } = cc._decorator;

/**
 * 节奏字逻辑类
 */
@ccclass
export default class SpriteWord extends cc.Component {


    @property(cc.Sprite)
    private spWord: cc.Sprite = null;


    @property(cc.Sprite)
    private spOk: cc.Sprite = null;

    @property(cc.Sprite)
    private spNo: cc.Sprite = null;

    @property(cc.Texture2D)
    private texWash: {
        default: null,
        type: cc.Texture2D
    };

    @property(cc.Texture2D)
    private texCut: {
        default: null,
        type: cc.Texture2D
    };

    @property(cc.Texture2D)
    private texFlow: {
        default: null,
        type: cc.Texture2D
    };

    private _type: LogicConst = LogicConst.Cut;
    private _time: number = 0;

    public getType(): LogicConst {
        return this._type;
    }

    public setTexture(type: LogicConst) {
        this._type = type;
        switch (type) {
            case LogicConst.Cut:
                this.spWord.spriteFrame.setTexture(this.texCut);
                break;
            case LogicConst.Flow:
                this.spWord.spriteFrame.setTexture(this.texFlow);
                break;
            case LogicConst.Wash:
                this.spWord.spriteFrame.setTexture(this.texWash);
                break;
        }
    }

    public ok() {
        this.spOk.node.active = true;
        this.spNo.node.active = false;
        setTimeout(function () {
            this.spOk.node.active = false;
        }.bind(this), 500);
        GameLogic.inst.delHair();
    }

    public no() {
        this.spNo.node.active = true;
        this.spOk.node.active = false;
        setTimeout(function () {
            this.spNo.node.active = false;
        }.bind(this), 500);
        GameLogic.inst.addHair();
    }

    /** 获取坐标 */
    public get pos(): cc.Vec2{
        return this.spWord.node.position;
    }

    /** 设置坐标 */
    public set pos(pos: cc.Vec2) {
        this.spWord.node.position = pos;
    }

    public get x(): number {
        return this.spWord.node.position.x;
    }

    public set x(x: number) {
        this.spWord.node.position.x = x;
    }

    public get y(): number {
        return this.spWord.node.position.x;
    }

    public set y(y: number) {
        this.spWord.node.position.y = y;
    }

    public get width(): number {
        return this.spWord.node.width;
    }


    onLoad() {
        /** node 设置active 为false后，update不会执行，这个要注意 */
        this.spWord.node.active = false;
        this.spOk.node.active = false;
        this.spNo.node.active = false;
    }

    /** 显示字 */
    showSpWord() {
        if (this.spWord.node.active) return;
        this.spWord.node.active = true;
    }

    /** 隐藏所有 */
    hideAll() {
        this.spWord.node.active = false;
        this.spOk.node.active = false;
        this.spNo.node.active = false;
    }

    start() {

    }

    update(dt) {
        if (GameStateMgr.inst.getState() != GameState.Start) {
            return;
        }
        if (!this.spWord.node.active) {
            this.spWord.node.active = true;
        }
        if (this._time > LogicConst.WordChangeInt) {
            this._time = 0;
            this.changeWordRandom();
        }
        this._time += dt;
    }

    private changeWordRandom() {
        this.setTexture([LogicConst.Cut, LogicConst.Flow, LogicConst.Wash][Math.floor(Math.random()*3)]);
    }

}
