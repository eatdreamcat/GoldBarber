import { GameStateMgr, GameState } from "../Controller/GameState";
import { GameLogic } from "../Controller/GameLogic";

const { ccclass, property } = cc._decorator;

/**
 * 角色逻辑类
 */
@ccclass
export default class Role extends cc.Component {


    @property(cc.Sprite)
    private spHair: cc.Sprite = null;

    
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.init();
    }

    init() {
        this.spHair.node.height = GameLogic.inst.getHairLen();
    }

    start() {

    }

    update(dt) {
        if (GameStateMgr.inst.getState() != GameState.Start) {
            return;
        }
        this.spHair.node.height = GameLogic.inst.getHairLen();
    }
}
