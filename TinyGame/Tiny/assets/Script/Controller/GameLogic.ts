import { GameStateMgr, GameState } from "./GameState";
import SpriteWord from "../items/SpriteWord";
import GameScene from "../Scene/GameScene";

/**
 * 逻辑相关常量
 */
export enum LogicConst {
    /** 初始头发长度 */
    InitHairLen = 400,
    /** 最短头发长度 */
    MinHairLen = 0,
    /** 最长头发长度 */
    MaxHairLen = 800,
    /** 头发加的步长 */
    AddHairLen = 100,
    /** 头发自然生长的速度 */
    NatureHairAdd = 1,
    /** 洗 */
    Wash = 1,
    /** 剪 */
    Cut = 2,
    /** 吹 */
    Flow = 3,
    /** 节奏字变换的频率 秒*/
    WordChangeInt = 1,
}
/**
 * 游戏逻辑单例
 */
export class GameLogic {
    private static _inst: GameLogic = null;
    public static get inst(): GameLogic {
        return this._inst ? this._inst : this._inst = new GameLogic();
    }

    /** 游戏得分 */
    private _score: number = 0;
    private _hairLength: number = LogicConst.InitHairLen;
    private _timerID: number = -1;

    
    /** 当前的节奏字 */
    private _curWord: SpriteWord = null;

    public init() {
        this._hairLength = LogicConst.InitHairLen;
        this._score = 0;
        this._curWord = cc.director.getScene().getChildByName('Canvas').getComponent('GameScene').getSpWord().getComponent('SpriteWord');
        if (this._timerID == -1) {
            this._timerID = setInterval(this.gameUpdate.bind(this), 100);
        }

       

    }

    public getHairLen(): number {
        return this._hairLength;
    }

    public getScore(): number {
        return this._score;
    }

    public setScore(score: number) {
        this._score = score;
    }

    /** 头发变长 */
    public addHair() {
        this._hairLength += LogicConst.AddHairLen;
        this.checkHair();
    }

    /** 头发变短 */
    public delHair() {
        this._hairLength -= LogicConst.AddHairLen;
        this.checkHair();
    }

    /** 判断头发长度 */
    private checkHair() {
        if (this._hairLength >= LogicConst.MaxHairLen) {
            GameStateMgr.inst.end();
            return;
        }

        if (this._hairLength <= LogicConst.MinHairLen) {
            this._hairLength = LogicConst.MinHairLen;
        }
    }

    private gameUpdate() {
        if (GameStateMgr.inst.getState() == GameState.Start) {
            this._curWord.showSpWord();
            this._hairLength += LogicConst.NatureHairAdd;
            this.checkHair();
        } else {
            this._curWord.hideAll();
        }
    }

    public cut() {
        if (GameStateMgr.inst.getState() != GameState.Start) {
            return;
        }
    }

    public wash() {
        if (GameStateMgr.inst.getState() != GameState.Start) {
            return;
        }
    }

    public flow() {
        if (GameStateMgr.inst.getState() != GameState.Start) {
            return;
        }
    }

}