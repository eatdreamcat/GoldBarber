/** 游戏状态 */
export enum GameState {
    /** 前期准备阶段 */
    Prepare,
    /** 准备完毕 */
    Ready,
    /** 开始游戏 */
    Start,
    /** 游戏暂停 */
    Stop,
    /** 游戏结束 */
    End
}

/**
 * 游戏状态管理 单例
 */
export class GameStateMgr {
    private static _inst: GameStateMgr = null;
    public static get inst(): GameStateMgr {
        return this._inst ? this._inst : this._inst = new GameStateMgr();
    }

    private _state: GameState = GameState.Prepare;

    public prepare() {
        this._state = GameState.Prepare;
    }

    public ready() {
        this._state = GameState.Ready;
    }

    public start() {
        this._state = GameState.Start;
    }

    public stop() {
        this._state = GameState.Stop;
    }

    public end() {
        this._state = GameState.End;
    }

    /** 当前游戏的状态 */
    public getState(): GameState {
        return this._state;
    }

}