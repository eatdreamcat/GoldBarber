import { SceneName } from "../Define/Define";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SplashScene extends cc.Component {

    @property(cc.Sprite)
    private logo: cc.Sprite = null;
    start () {
        cc.director.preloadScene(SceneName.Game);
        let ani: cc.Animation = this.logo.getComponent(cc.Animation);
        ani.play().wrapMode = cc.WrapMode.Normal;
        ani.once('finished', this.gotoGameScene, this);
    }

    gotoGameScene() {
        cc.director.loadScene(SceneName.Game);
    }
}
