import { GameLogic } from "../Controller/GameLogic";
import { GameStateMgr } from "../Controller/GameState";
import Role from "../items/Role";

const { ccclass, property } = cc._decorator;
/**
 * 场景逻辑类
 */
@ccclass
export default class GameScene extends cc.Component {


    @property(cc.Button)
    private btnStart: cc.Button = null;

    @property(cc.Button)
    private btnRestart: cc.Button = null;

    @property(cc.Button)
    private btnCut: cc.Button = null;

    @property(cc.Button)
    private btnWash: cc.Button = null;

    @property(cc.Button)
    private btnFlow: cc.Button = null;

    @property(cc.Label)
    private lblScore: cc.Label = null;

    @property(cc.Sprite)
    private spRole: cc.Label = null;

    @property(cc.Sprite)
    private spWord: cc.Sprite = null;

    onLoad() {

        GameLogic.inst.init();
        GameStateMgr.inst.ready();

        this.btnWash.node.on('click', ()=>{
            GameLogic.inst.wash();
        }, this);

        this.btnCut.node.on('click', ()=>{
            GameLogic.inst.cut();
        }, this);

        this.btnFlow.node.on('click', ()=>{
            GameLogic.inst.flow();
        }, this);

        this.btnStart.node.on('click', ()=>{
            GameStateMgr.inst.start();
        }, this);

        this.btnRestart.node.on('click', ()=>{
            GameStateMgr.inst.ready();
            GameLogic.inst.init();
            (this.spRole.getComponent('Role') as Role).init();
        }, this);
    }

    getSpWord() {
        return this.spWord;
    }

    start() {

    }


    update(dt) {

    }
}
