/** 场景名称 */
export const SceneName = {
    Splash: 'SplashScene',
    Game: 'GameScene'
}

/** 碰撞体tag */
export const enum ColliderTag  {
    /** 地板 */
    Ground,
    /** 盒子 */
    Box,
    /** 角色 */
    Role
}

